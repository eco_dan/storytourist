<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ECST_API_Controller {

	private $_new_accounts = [];

	function __construct() {
		add_action( 'rest_api_init', [ $this, 'register_routes' ] );
	}

	function register_routes() {
		register_rest_route(
			'storytourist/v1',
			'/story',
			[
				[
					'methods'  => 'POST',
					'callback' => [
						$this,
						'story'
					]
				],
				[
					'methods'  => 'GET',
					'callback' => [
						$this,
						'story'
					]
				],
				[
					'methods'  => 'PUT',
					'callback' => [
						$this,
						'story'
					]
				]
			]
		);
		register_rest_route(
			'storytourist/v1',
			'/user/(?P<username>[^/]+)',
			[
				'methods'  => 'GET',
				'callback' => [
					$this,
					'user'
				]
			]
		);
//		register_rest_route(
//			'storytourist/v1',
//			'/user/(?P<username>[^/]+)/add_product/(?P<product_id>[^/]+)',
//			[
//				'methods'  => 'GET',
//				'callback' => [
//					$this,
//					'add_product'
//				]
//			]
//		);
		register_rest_route(
			'storytourist/v1',
			'/user/(?P<username>[^/]+)/remove_product/(?P<product_id>[^/]+)',
			[
				'methods'  => 'GET',
				'callback' => [
					$this,
					'remove_product'
				]
			]
		);
		register_rest_route(
			'storytourist/v1',
			'/story/(?P<product>[^/]+)',
			[
				'methods'  => 'GET',
				'callback' => [
					$this,
					'list_product'
				]
			]
		);
	}

	function story( WP_REST_Request $request ) {
		$response = [ 'action' => 'story' ];
		$data     = $request->get_json_params();
		if ( isset( $data['activityBookings'] ) ) {
			foreach ( $data['activityBookings'] as $activity ) {
//				$gift = false;
				if ( isset( $activity['productId'] ) && isset( $activity['pricingCategoryBookings'] ) ) {
					foreach ( $activity['pricingCategoryBookings'] as $pcb ) {
//						if ( isset( $activity['answers'] ) ) {
//							foreach ( $activity['answers'] as $answer ) {
//								if (
//									isset( $answer['question'] ) && $answer['question'] == 'Gift'
//									&& isset( $answer['answer'] ) && $answer['answer'] == 'true'
//								) {
//									$gift = true;
//								}
//							}
//						}
						if ( isset( $pcb['passengerInfo'] ) ) {
							$customer_data      = ! empty( $pcb['passengerInfo']['email'] ) ? $pcb['passengerInfo'] : $data['customer'];
							$customer           = $this->get_customer( $customer_data['email'], $customer_data );
							$entry['recipient'] = $customer_data['email'];
							$customer->add_new_product( $activity['productId'] );
							$entry['product']      = $activity['productId'];
							$response['entries'][] = $entry;
						}
					}
				}
			}
		}

		// TODO: Send mail
		if( count($this->_new_accounts) != 0) {
			$message = '';
			foreach ($this->_new_accounts as $account) {
				$message .= "<tr><td>{$account['username']}</td><td>{$account['password']}</td></tr>";
			}
			ECST_Mail_Controller::send_email($data['customer']['email'], $message);
			$this->_new_accounts = [];
		}
	}

	function user( WP_REST_Request $request ) {
		$customer = $this->get_customer( $request['username'] );

		$result = $customer->get_product_ids();
		$this->send_response( [
			'action' => 'user',
			'productIds' => $result
		] );
	}

	function add_product( WP_REST_Request $request ) {
		$customer = $this->get_customer( $request['username'] );
		$customer->add_new_product( $request['product_id'] );
		$result = "Product {$request['product_id']} added for customer {$request['username']}";
		$this->send_response( [
			'action' => 'add_product',
			'result' => $result
		] );
	}

	function remove_product( WP_REST_Request $request ) {
		$customer = $this->get_customer( $request['username'] );
		$customer->remove_product( $request['product_id'] );
		$result = "Product {$request['product_id']} removed for customer {$request['username']}";
		$this->send_response( [
			'action' => 'remove_product',
			'result' => $result
		] );
	}

	function list_product( WP_REST_Request $request ) {
		$story = new ECST_Story( $request['product'] );
		$result = $story->get_customer_ids();
		$users = [];
		foreach (get_users(['include' => $result]) as $user) {
			/** @var $user WP_User */
			$users[] = $user->user_email;
		}

		$this->send_response( [
			'action' => 'list_product',
			'customerList' => $users
		] );
	}

	function get_customer( $username, $data = false ) {
		$user_id = email_exists( $username );

		if ( ! $user_id ) {
			$user_id = username_exists( $username );
		}

		if ( ! $user_id ) {
			$password              = wp_generate_password();
			$user_id               = wp_create_user( $username, $password );
			$this->_new_accounts[] = [ 'username' => $username, 'password' => $password];

			if ( $data ) {
				$userdata               = [];
				$userdata['ID']         = $user_id;
				$userdata['user_email'] = $data['email'];
				$userdata['first_name'] = $data['firstName'];
				$userdata['last_name']  = $data['lastName'];
				wp_update_user( $userdata );
			}
		}

		return new ECST_Customer( $user_id );
	}

	function get_product( $id ) {
		$product = get_posts( [
			'post_type'  => 'ecst_story',
			'meta_key'   => '_ecst_product_id',
			'meta_value' => $id
		] );

		if ( count( $product ) == 0 ) {
			$post_id = wp_insert_post( [
				'post_type' => 'ecst_story'
			] );
			if ( ! is_wp_error( $post_id ) && $post_id != 0 ) {
				update_post_meta( $post_id, '_ecst_product_id', $id );
				$product = get_post( $post_id );
			}
		} else {
			$product = $product[0];
		}

		return $product;
	}

	function validate_callback( $param, $request, $key ) {
		return ! empty( $param );
	}

	function send_response( $output ) {
		$json = [
			'time' => date('Y.m.d - H:m')
		];
		$response = array_merge($json, $output);
		wp_send_json($response);
	}
}

new ECST_API_Controller();