<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ECST_Mail_Controller {

	public static function send_gift_email( $to, $from ) {
		$subject = '';
		$message = '';
		$headers = '';
		wp_mail($to, $subject, $message, $headers);
	}

	public static function send_email( $to, $user_table ) {
		$subject = __('StoryTourist account details', 'ec_storytourist');
		$message = __('The following StoryTourist accounts have been created:', 'ec_storytourist');
		$message .= sprintf("<table><tr><th>%s</th><th>%s</th></tr>", __('User name', 'ec_storytourist'), __('Password', 'ec_storytourist'));
		$message .= $user_table;
		$message .= "</table>";
		$headers = [ 'Content-Type: text/html; charset=UTF-8' ];
		wp_mail($to, $subject, $message, $headers);
	}
}