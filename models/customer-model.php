<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ECST_Customer {

	private $_user_id;

	public function __construct( $user_id ) {
		$this->_user_id = $user_id;
	}

	public function get_products() {
		$args     = [
			'post__in' => $this->get_product_ids()
		];
		$products = get_posts( $args );

		return is_wp_error( $products ) ? [] : $products;
	}

	public function has_product( $product_id ) {

		foreach ( $this->get_product_ids() as $id ) {
			if ( $id == $product_id ) {
				return true;
			}
		}

		return false;
	}

	public function get_product_ids() {
		$product_ids = get_user_meta( $this->_user_id, '_ecst_booking' );
		if ( empty( $product_ids ) ) {
			return [];
		}

		return $product_ids;
	}

	public function add_new_product( $product_id ) {
		return add_user_meta( $this->_user_id, '_ecst_booking', $product_id );
	}

	public function remove_product( $product_id ) {
		return delete_user_meta( $this->_user_id, '_ecst_booking', $product_id );
	}
}