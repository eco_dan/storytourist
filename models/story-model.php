<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ECST_Story {

	private $_story_id;

	public function __construct( $story_id ) {
		$this->_story_id = $story_id;
	}

	public function get_customer_ids() {
		global $wpdb;
		$query = "SELECT user_id FROM wp_usermeta WHERE meta_key='_ecst_booking' AND meta_value='{$this->_story_id}'";
		$result = [];

		foreach ($wpdb->get_results($query) as $customer) {
			$result[] = $customer->user_id;
		}

		return $result;
	}
}