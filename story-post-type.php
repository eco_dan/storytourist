<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ECST_Story_Post_Type {

	function __construct() {
		add_action( 'init', [$this, 'register'] );
		add_action( 'admin_init', [ $this, 'register_meta_box' ] );
		add_action( 'save_post', [ $this, 'save' ], 10, 2 );
	}

	function register() {
		if ( ! post_type_exists( 'ecst_story' ) ) {
			$labels = array(
				'name'               => _x( 'Stories', 'post type general name', 'ec_storytourist' ),
				'singular_name'      => _x( 'Story', 'post type singular name', 'ec_storytourist' ),
				'add_new'            => _x( 'Add Story', 'book', 'ec_storytourist' ),
				'add_new_item'       => __( 'Add New Story', 'ec_storytourist' ),
				'edit_item'          => __( 'Edit Story', 'ec_storytourist' ),
				'new_item'           => __( 'New Story', 'ec_storytourist' ),
				'all_items'          => __( 'Stories', 'ec_storytourist' ),
				'view_item'          => __( 'View Story', 'ec_storytourist' ),
				'search_items'       => __( 'Search Stories', 'ec_storytourist' ),
				'not_found'          => __( 'No Stories found', 'ec_storytourist' ),
				'not_found_in_trash' => __( 'No Stories found in the Trash', 'ec_storytourist' ),
				'parent_item_colon'  => '',
				'menu_name'          => 'Stories',
			);
			$args   = array(
				'labels'              => $labels,
				'description'         => 'Story',
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'capability_type'     => 'post',
				'rewrite'             => true,
				'menu_icon'           => 'dashicons-format-image',
				'supports'            => array(
					'title'
				),
				'has_archive'         => false,
				'publicly_queryable'  => true,
				'exclude_from_search' => true,
			);
			register_post_type( 'ecst_story', $args );

			add_filter( 'post_updated_messages', [$this, 'messages'] );
			add_filter( 'manage_ecst_story_posts_columns', [$this, 'columns'] );
			add_action( 'manage_ecst_story_posts_custom_column', [$this, 'render_columns'], 2 );
		}
	}

	function messages( $messages ) {
		return $messages;
	}

	function columns( $columns ) {
		$new = [];
		
		if ( isset( $columns['title'] ) ) {
			$new['title'] = $columns['title'];
		}

		$new['product_id'] = __( 'Product ID', 'ec_storytourist' );

		if ( isset( $columns['date'] ) ) {
			$new['date'] = $columns['date'];
		}

		return $new;
	}

	function render_columns( $column ) {
	    global $post;
	    if($column == 'product_id') {
	        echo get_post_meta($post->ID, '_ecst_product_id', true);
        }
	}

	/**
	 * @param int|string $post_id
	 * @param WP_Post $post
	 */
	function save( $post_id, $post ) {
		if ( $post->post_type == 'ecst_story' && isset( $_POST['product_id'] ) ) {
			update_post_meta( $post_id, '_ecst_product_id', $_POST['product_id'] );
		}
	}

	function register_meta_box() {
		add_meta_box(
			'ecst_story_meta_box',
			__( 'Story', 'ec_storytourist' ),
			[ $this, 'meta_box_callback' ],
			'ecst_story', 'normal', 'high'
		);
	}

	/**
	 * @param WP_Post $post
	 */
	function meta_box_callback( $post ) {
	    $product_id = get_post_meta($post->ID, '_ecst_product_id', true);
		?>
		<div id="ecst_wrapper">
			<label for="product_id"><?php _e('Product ID: '); ?></label>
			<input type="text" id="product_id" name="product_id" <?php echo !empty($product_id) ? "value=\"{$product_id}\"" : ""; ?> >
		</div>
		<?php
	}
}

new ECST_Story_Post_Type();