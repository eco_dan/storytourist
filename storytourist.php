<?php
/*
Plugin Name: StoryTourist
Plugin URI: http://www.ecomatic.se/
Version: 0.1
Description:
Author: Ecomatic
Author URI: http://www.ecomatic.se/
License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Text Domain: ec_storytourist
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class ECST_Plugin_Controller {

	function __construct() {
		load_plugin_textdomain( 'ec_storytourist', false, 'storytourist' );

		$this->includes();
		$this->constants();
	}

	function includes() {
		include 'api-controller.php';
		include 'mail-controller.php';
		include 'story-post-type.php';
		include 'models/customer-model.php';
		include 'models/story-model.php';
	}

	function constants() {
		define( 'ECST_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
		define( 'ECST_PLUGIN_URL', untrailingslashit( plugins_url( '/', __FILE__ ) ) );
	}
}

new ECST_Plugin_Controller();